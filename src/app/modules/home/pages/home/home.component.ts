import { Component, OnInit, AfterViewInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { SettingService } from 'src/app/core/services/setting.service';
import { DashboardService } from './../../../../core/services/dashboard.service';
import { AuthenticationService } from './../../../../core/services/authentication.service';
import { UserService } from './../../../../core/services/user.service';

import { environment } from '../../../../../environments/environment';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {
	private apiUrl = environment.apiUrl;
	public modalRef: BsModalRef;
	isPageLoaded: boolean = false;
	settings: any;
	loginUser: any;
	dashboardLists: any;
	dashboardLists1: any;
	length = 5;
	userLists = [];

	constructor(
		private modalService: BsModalService,
		private authenticationService: AuthenticationService,
		private dashboardService: DashboardService,
		private userService: UserService,
		private settingService: SettingService
	) {
		this.authenticationService.loginUser.subscribe(x => this.loginUser = x);
	}

	ngOnInit() {
		this.getSettings();
		this.getDashboardCounts(this.length);
		this.getUsers();
	}

	getSettings() {
		this.settingService.getAll()
			.subscribe(
				data => {
					this.settings = data;
				});
	}

	getUsers() {
		this.userService.getUserkeyBy().subscribe(data => {
			this.userLists = data;
		});
	}

	getDashboardCounts(length) {
		this.dashboardService.getDashboardCounts(length)
			.subscribe(
				data => {
					this.dashboardLists = data;
					this.getDashboardLists(this.length);
				});
	}

	getDashboardLists(length) {
		this.dashboardService.getDashboardLists(length)
			.subscribe(
				data => {
					this.dashboardLists1 = data;
					this.isPageLoaded = true;
				});
	}
}
